Spring Boot REST APIs(v1.0.0)
==========
* Java 8
* Spring Boot(with Spring Security, Spring Web, Spring JPA)
* MySQL
* Maven 3.6.1
* JWT Authentication

Run Spring Boot application with command 
==========
```
$ ./mvnw spring-boot:run
```

APIs that Spring Boot App will export:
==========

| Methods | URLs |Actions|
| ------ | ------ |------ |
| POST	 | /api/auth/signup |signup new account |
| POST	 | /api/auth/signin |login an account |
| POST	 | /api/tutorial |create new tutorial |
| GET	 | /api/tutorial |retrieve all tutorials |
| GET	 | /api/tutorial/:id |retrieve a tutorial by :id |
| PUT	 | /api/tutorial/:id |update a tutorial by :id |
| DELETE | /api/tutorial/:id |delete a Tutorial by :id |
| DELETE  | /api/tutorial |delete all tutorial |
| GET    | /api/tutorials?title=[keyword]|find all Tutorials which title contains keyword |

User interfave is designed with Ionic/Angular:
==========
https://gitlab.com/lijinyanbest/ionic-angular-mobile-app.git